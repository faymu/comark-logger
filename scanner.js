import fetch from 'node-fetch'
import { format, differenceInHours, differenceInMinutes, differenceInSeconds } from 'date-fns'
import _ from 'lodash'
const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const ipc = require('node-ipc')
const parser = new Readline({ delimiter: '\r\n' })

ipc.config.id = 'scanner'
ipc.config.retry = 1500
ipc.config.silent = true

const formatTime = (time) => { 
  if (time < 10) {
    return '0' + time
  }
}

const sendDataAndUpdateLogs = async (sp, teacherJson, room, lastLog) => {
  await fetch('http://localhost:3000/updateRoomStatus', {
    method: 'POST',
    body: JSON.stringify(room),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  fetch('http://localhost:3000/logAction', { 
    method: 'POST',
    body: JSON.stringify(room),
    headers: {
      'Content-Type': 'application/json'
    } 
  })
  const name = teacherJson.lastName + ',' + teacherJson.firstName
  const dateNow = new Date()
  const time = format(dateNow, 'hh:mmA')
  sp.write(Buffer.from(name, 'ascii'), (err, result) => {
    if (err) {
      console.log("error", err)
    }
  })
  sp.write(Buffer.from('|' + time + '(', 'ascii'), (err, result) => {
    if (err) {
      console.log("error", err)
    }
  })
  sp.write(Buffer.from(room.status + ')', 'ascii'), (err, result) => {
    if (err) {
      console.log("error", err)
    }
  })
  if (room.status == 'off' && lastLog) {
    console.log(differenceInSeconds(dateNow, lastLog.date))
    const diff = formatTime(differenceInHours(dateNow, lastLog.date)) + ':' 
    + formatTime(differenceInMinutes(dateNow, lastLog.date)) 
    // + formatTime(differenceInSeconds(dateNow, lastLog.date))
    sp.write(Buffer.from( diff + '*', 'ascii'), (err, result) => {
      if (err) {
        console.log("error", err)
      }
    })
  }
}

SerialPort.list((error, ports) => {
  let count = 0;
  let isDone = false
  if (error) {
    console.log(error.message)
  } else {
    ports.forEach((port) => {
      count += 1
      let pm = port['manufacturer']
      if (typeof pm !== 'undefined') {
        console.log(port)
        let arduinoPort = port.comName.toString()
        const sp = new SerialPort(arduinoPort, {
          baudRate: 9600
        })
        sp.pipe(parser)
        sp.on('open', error => {
          if (error) {
            console.log(error.message)
          }
          ipc.connectTo('server', () => {
            console.log('ID scanner is connecting to the server.')
            ipc.of.server.on('connect', () => {
              console.log('ID scanner has connected to the server.')
              parser.on('data', async (data) => {
                if (_.startsWith(data, '-')) {
                  ipc.of.server.emit('energy', data)
                } else {
                  ipc.of.server.emit('scannedid', data)
                  console.log('port', arduinoPort)
                  if (arduinoPort == 'COM5') {
                    console.log('CALLED it')
                    const teacher = await fetch('http://localhost:3000/getTeacherDataByRfidCode?rfidCode=' + data)
                    const teacherJson = await teacher.json()
                    console.log('TEACHER', teacherJson)
                    const room = await fetch('http://localhost:3000/getRoomStatus')
                    const roomJson = await room.json()
                    console.log('ROOM', roomJson)
                    const logs = await fetch('http://localhost:3000/getLogs')
                    const logsJson = await logs.json()
                    const lastLog = logsJson[logsJson.length - 1]
                    // if off the last log does not matter
                    // however if it is on check if the rfidCode of the last log matches the rfidCode of the scanned ID
                    // if yes turn it off and log  it
                    if (roomJson.status == 'off') {
                      roomJson.status = 'on'
                      console.log('ON')
                      sendDataAndUpdateLogs(sp, teacherJson, roomJson)
                    } else {
                      if (lastLog && data == lastLog.rfid) {
                        roomJson.status = 'off'
                        console.log('OFF')
                        sendDataAndUpdateLogs(sp, teacherJson, roomJson, lastLog)
                      }
                    }
                  }
                }
              })
            })
          })
          console.log('Done! Arduino is now connected at port: ' + arduinoPort)
        })
        isDone = true
      }
      if (count === ports.length && isDone === false) {
        console.log('Arduino not found! Check if it is plugged in correctly.')
      }
    })
  }
})

