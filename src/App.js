import React from 'react'
import './App.css'
import { Menu, Segment, Grid } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css'
import AddTeacherForm from './redux/containers/AddTeacherForm'
import TeachersOverview from './redux/containers/TeachersOverview'
import EditTeacherModal from './redux/containers/EditTeacherModal'
import LogsOverview from './redux/containers/LogsOverview'


const submit = (rfidCode, registerTeacher, values) => {
  const teacher = { ...values, rfidCode: rfidCode }
  registerTeacher(teacher)
}

const App = ({ rfidCode, registerTeacher, activeItem, setActiveItem }) => {
  return (
    <div className="App-header">
      <EditTeacherModal />
      <Grid style={ styles.grid }>
        <Grid.Column width={4}>
          <Menu vertical tabular fluid color={'grey'} inverted size={'large'}>
            <Menu.Item
              content='Register New Teacher'
              active={activeItem === 'register'}
              onClick={() => setActiveItem('register')}
            />
            <Menu.Item
              content='Overview of Registered Teachers'
              active={activeItem === 'overview'}
              onClick={() => setActiveItem('overview')}
            />
            <Menu.Item
              content='Activity Log'
              active={activeItem === 'logs'}
              onClick={() => setActiveItem('logs')}
            />
          </Menu>
        </Grid.Column>

        <Grid.Column stretched width={12}>
          <Segment style={ styles.segment }>
            {activeItem === 'overview' && <TeachersOverview />}
            {activeItem === 'logs' && <LogsOverview />}
            {activeItem === 'register' && <AddTeacherForm onSubmit={(values) => submit(rfidCode, registerTeacher, values)} />}
          </Segment>
        </Grid.Column>
      </Grid>
    </div>
  )
}

const styles = {
  grid: {
    margin: 0,
    minHeight: '100vh',
    paddingLeft: '5vw',
    paddingRight: '5w'
  },
  segment: {
    paddingLeft: 50,
    paddingRight: 50,
    maxHeight: '96vh',
    overflowY: 'auto'
  }
}

export default App
