import React, { Component } from 'react'
import { Table, TableCell, TableRow, TableBody, Segment, Popup, Header, Divider } from 'semantic-ui-react'
import { format } from 'date-fns'

class Overview extends Component {
  componentDidMount() {
    this.props.getLogs()
  }

  render() {
    const rows = this.props.logs.map((log, index) => {
      return (
        <TableRow key={index} positive={log.status === 'on'} negative={log.status === 'off'}>
          <Popup
            trigger={<TableCell><b>{log.teacher.lastName}, {log.teacher.firstName}</b></TableCell>}
            content={
              <Segment basic style={{ padding: 0 }}>
                <Header as={'h4'} style={{ margin: 0 }}>
                  {log.teacher.idNumber}
                </Header>
                <Divider fitted style={{ marginBottom: 5 }} />
              </Segment>
            }
          />
          <TableCell>{log.roomNumber}</TableCell>
          <TableCell>{log.status}</TableCell>
          <TableCell>{format(log.date, 'MMMM DD, YYYY')}</TableCell>
          <TableCell>{format(log.date, 'hh:mm A')}</TableCell>
          <Table.Cell>
            <strong>
              {log.status === 'off' ? '0.0' :  ''}
            </strong>
          </Table.Cell>
        </TableRow >
      )
    })

    return (
      <Table celled fixed textAlign={'center'} selectable basic={'very'} style={{ overflowY: 'auto' }}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Employee</Table.HeaderCell>
            <Table.HeaderCell>Room</Table.HeaderCell>
            <Table.HeaderCell>Status</Table.HeaderCell>
            <Table.HeaderCell>Date</Table.HeaderCell>
            <Table.HeaderCell>Time</Table.HeaderCell>
            <Table.HeaderCell>Power Consumption</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <TableBody>
          {rows}
        </TableBody >
      </Table >
    )
  }
}

export default Overview