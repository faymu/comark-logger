import React from 'react'
import { Modal } from 'semantic-ui-react'
import AddTeacherForm from '../redux/containers/AddTeacherForm'

const EditModal = ({ isOpen, initialValues, toggleEditTeacherModal, updateTeacherData }) => {
  return (
    <Modal open={isOpen} style={{ padding: 20 }} basic>
      <AddTeacherForm 
        initialValues={initialValues} 
        toggleEditTeacherModal={toggleEditTeacherModal}
        onSubmit={(values) => updateTeacherData(values)}
      />
    </Modal>
  )
}

export default EditModal