import React from 'react'
import { Field } from 'redux-form'
import { Form, Button, Header, Image, Segment, Divider, Dimmer, Loader } from 'semantic-ui-react'
import { DropdownField } from './custom/Dropdown'
import { employmentTypes, departments } from '../constants'

const AddTeacherForm = ({ handleSubmit, pristine, submitting, getScannedId, rfidCode, isScanning, initialValues, toggleEditTeacherModal }) => {
	const content = rfidCode ? rfidCode : 'Click the button below to scan your RFID-enabled identification card on the scanner.'
	const isInverted = initialValues != null
	return (
		<Form onSubmit={handleSubmit} autoComplete={'off'} size={'small'}>
			<Form.Group widths={'equal'}>
				<Form.Field style={styles.formField}>
					<label>
						<Header size={'small'} inverted={isInverted} style={styles.header}>
							First Name
						</Header>
					</label>
					<Field 
						name={'firstName'} 
						component={'input'} 
						type={'text'} 
						required 
					/>
				</Form.Field>
				<Form.Field style={styles.formField}>
					<label>
						<Header size={'small'} inverted={isInverted} style={styles.header}>
							Last Name
						</Header>
					</label>
					<Field 
						name={'lastName'} 
						component={'input'} 
						type={'text'} 
						required 
					/>
				</Form.Field>
			</Form.Group>
			<Form.Group widths={'equal'}>
				<Form.Field style={styles.formField}>
					<label>
						<Header size={'small'} inverted={isInverted} style={styles.header}>
							ID Number
						</Header>
					</label>
					<Field
						name={'idNumber'}
						component={'input'}
						type={'text'}
						style={styles.field}
						required
					/>
				</Form.Field>
				<Form.Field style={styles.formField}>
					<label>
						<Header size={'small'} inverted={isInverted} style={styles.header}>
							Department
						</Header>
					</label>
					<Field
						name={'department'}
						component={DropdownField}
						label={'Select Department'}
						options={departments}
						required
					/>
				</Form.Field>
			</Form.Group>
			<Form.Group style={{ width: '50%', marginLeft: '25%' }}>
				<Form.Field style={styles.formField}>
					<label>
						<Header size={'small'} inverted={isInverted} style={styles.header}>
							Employment Type
						</Header>
					</label>
					<Field
						name={'employmentType'}
						component={DropdownField}
						label={'Select Employment Type'}
						options={employmentTypes}
						required
					/>
				</Form.Field>
			</Form.Group>
			{!initialValues && <Form.Field style={{ width: '50%', marginLeft: '25%' }}>
				<Segment style={{ padding: 10 }} textAlign={'center'} basic>
					{isScanning && <Dimmer active inverted>
						<Loader size={'huge'}>Scanning RFID Card...</Loader>
					</Dimmer>}
					<Header icon style={{ margin: 0 }}>
						<Image src={'./rfid.png'} size={'massive'} /><br />
						{content}
					</Header>
					<Divider />
					<Button primary type={'button'} onClick={() => getScannedId()}>Scan RFID Card</Button>
				</Segment>
			</Form.Field>}
			<div style={{ textAlign: 'center' }}>
				{initialValues && <Divider />}
				<Button type={'submit'} disabled={pristine || submitting}>
					{initialValues ? 'Save Changes' : 'Submit'}
				</Button>
				{initialValues && <Button onClick={() => toggleEditTeacherModal()}>Cancel</Button>}
			</div>
		</Form>
	)
}

const styles = {
	formField: {
		width: '100%'
	},
	header: {
		margin: 0
	}
}

export default AddTeacherForm
