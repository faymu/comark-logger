import { connect } from 'react-redux'
import TeachersOverview from '../../components/TeachersOverview'
import { getTeachers, toggleEditTeacherModal, deleteTeacher } from '../actions/index'

function mapStateToProps(state) {
  return {
    teachers: state.teachers.teachers
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getTeachers() {
      dispatch(getTeachers())
    },
    toggleEditTeacherModal(id) {
      dispatch(toggleEditTeacherModal(id))
    },
    deleteTeacher(teacher) {
      dispatch(deleteTeacher(teacher))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TeachersOverview)