import { connect } from 'react-redux'
import { registerTeacher, setActiveItem } from '../actions/index'
import App from '../../App'

function mapStateToProps(state) {
  return {
    rfidCode: state.teacherRegistration.rfidCode,
    activeItem: state.menu.activeItem
  }
}

function mapDispatchToProps(dispatch) {
  return {
    registerTeacher(values) {
      dispatch(registerTeacher(values))
    },
    setActiveItem(item) {
      dispatch(setActiveItem(item))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
