import { reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import AddTeacherForm from '../../components/AddTeacherForm'
import { getScannedId } from '../actions/index'

function mapStateToProps(state) {
  return {
    rfidCode: state.teacherRegistration.rfidCode,
    isScanning: state.teacherRegistration.isScanning
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getScannedId() {
      dispatch(getScannedId())
    }
  }
}

const Form = reduxForm({ form: 'addTeacherForm', enableReinitialize: true })(AddTeacherForm)

export default connect(mapStateToProps, mapDispatchToProps)(Form)