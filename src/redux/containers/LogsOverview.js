import { connect } from 'react-redux'
import LogsOverview from '../../components/LogsOverview'
import { getLogs } from '../actions/index'

function mapDispatchToProps(dispatch) {
  return {
    getLogs() {
      dispatch(getLogs())
    }
  }
}

function mapStateToProps(state) {
  return {
    logs: state.logs.logs
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LogsOverview)