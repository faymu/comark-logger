import reducers from './reducers/index'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'
import { createStore, applyMiddleware } from 'redux'

export default createStore(reducers, applyMiddleware(promise, thunk))
