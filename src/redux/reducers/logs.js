const initialState = {
  logs: []
}

export default function reducer(state = initialState, action) {
  switch(action.type) {
    case 'GET_LOGS_SUCCESS':
      return {
        ...state,
        logs: action.payload
      }
    default: 
      return {
        ...state
      }
  }
}