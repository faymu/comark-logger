const initialState = {
  isOpen: false,
  initialValues: null
}

export default function reducer(state = initialState, action) {
  switch(action.type) {
    case 'TOGGLE_EDIT_MODAL':
      return {
        ...state,
        isOpen: !state.isOpen
      }
    case 'GET_TEACHER_DATA_SUCCESS':
      return {
        ...state,
        initialValues: action.payload
      }
    case 'UPDATE_TEACHER_DATA_SUCCESS':
      return {
        ...state,
        initialValues: null,
        isOpen: false
      }
    default: 
      return {
        ...state
      }
  }
}