const initialState = {
  teachers: []
}

export default function reducer(state = initialState, action) {
  switch(action.type) {
    case 'FETCH_ALL_TEACHERS_SUCCESS':
      return {
        ...state,
        teachers: action.payload
      }
    default: 
      return {
        ...state
      }
  }
}