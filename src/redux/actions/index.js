import { reset } from 'redux-form'
import iziToast from 'izitoast'

export function getScannedId() {
  return async dispatch => {    
    let id = ''
    dispatch({
      type: 'SCANNING_RFID',
    })
    try {
      while (id === '') {
        const response = await fetch('http://localhost:3000/scanned-id', {
          headers: {
            'Content-Type': 'application/json'
          }
        })
        id = await response.json()
      }
      dispatch({
        type: 'RFID_SCAN_SUCCESS',
        payload: id
      })
    } catch (e) {
      dispatch({
        type: 'RFID_SCAN_FAILURE',
        payload: e.message
      })
    }
  }
}

export function registerTeacher(values) {
  return async dispatch => {
    try {
      await fetch('http://localhost:3000/registerTeacher', {
        method: 'POST',
        body: JSON.stringify(values),
        headers: {
          'Content-Type': 'application/json'
        }
      })
      dispatch({ type: 'REGISTER_TEACHER_SUCCESS' })
      iziToast.success({
        title: 'SUCCESS!',
        message: 'User added successfully',
        position: 'topRight'
      })
      dispatch(reset('addTeacherForm'))
    } catch (e) {
      console.log(e.message)
    }
  }
}

export function getTeachers() {
  return async dispatch => {
    try {
      const response = await fetch('http://localhost:3000/getTeachers', {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      const teachers = await response.json()
      dispatch({
        type: 'FETCH_ALL_TEACHERS_SUCCESS',
        payload: teachers
      })
    } catch (e) {
      console.log(e.message)
    }
  }
}

export function setActiveItem(item) {
  return dispatch => {
    dispatch({
      type: 'SET_ACTIVE_ITEM',
      payload: item
    })
  }
}

export function toggleEditTeacherModal(id) {
  return dispatch => {
    if (id) {
      dispatch(getTeacherData(id))
    }
    dispatch({
      type: 'TOGGLE_EDIT_MODAL'
    })
  }
}

export function getTeacherData(id) {
  return async dispatch => {
    const url = 'http://localhost:3000/getTeacherData?id=' + id
    try {
      const response = await fetch(url)
      const teacher = await response.json()
      dispatch({
        type: 'GET_TEACHER_DATA_SUCCESS',
        payload: teacher
      })
    } catch (e) {
      console.log('error', e.message)
    }
  }
}

export function updateTeacherData(values) {
  return async dispatch => {
    try {
      await fetch('http://localhost:3000/updateTeacherData', {
        method: 'POST',
        body: JSON.stringify(values),
        headers: {
          'Content-Type': 'application/json'
        }
      })
      dispatch({
        type: 'UPDATE_TEACHER_DATA_SUCCESS'
      })
      iziToast.success({
        title: 'SUCCESS!',
        message: 'User updated successfully',
        position: 'topRight'
      })
      dispatch(getTeachers())
    } catch (e) {
      console.log('error', e.message)
    }
  }
}

export function getLogs() {
  return async dispatch => {
    try {
      const response = await fetch('http://localhost:3000/getLogs')
      const logs = await response.json()
      for (const log of logs) {
        const url = 'http://localhost:3000/getTeacherDataByRfidCode?rfidCode=' + log.rfid
        const response = await fetch(url)
        const data = await response.json()
        log.teacher = data
      }
      dispatch({
        type: 'GET_LOGS_SUCCESS',
        payload: logs
      })
    } catch (e) {
      console.log('error', e.message)
    }
  }
}

export function deleteTeacher(teacher) {
  return async dispatch => {
    try {
      await fetch('http://localhost:3000/removeTeacher', {
        method: 'POST',
        body: JSON.stringify({ id: teacher._id }),
        headers: {
          'Content-Type': 'application/json'
        }
      })
      await fetch('http://localhost:3000/removeUserLogs', {
        method: 'POST',
        body: JSON.stringify({ rfidCode: teacher.rfidCode }),
        headers: {
          'Content-Type': 'application/json'
        }
      })
      iziToast.success({
        title: 'SUCCESS!',
        message: 'User removed successfully',
        position: 'topRight'
      })
      dispatch(getTeachers())
    } catch (e) {
      console.log('error', e.message)
    }
  }
}