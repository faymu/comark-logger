import express from 'express'
import cors from 'cors'
import MongoClient from 'mongodb'

const app = express()
const port = 3000 
const url = 'mongodb://localhost:27017'
const dbName = 'activity-logger'
const ObjectId = MongoClient.ObjectID
let db

MongoClient.connect(url, { useNewUrlParser: true }, (error, client) => {
  if(error) {
    console.log(error.message)
  } else {
    db = client.db(dbName)
    console.log('Connected successfully to the database.')
  }
  // client.close()
})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.get('/scanned-id', (request, response) => {
  response.json(scannedId)
})

app.post('/logAction', (request, response) => {
  console.log('logged')
  const room = request.body
  console.log(scannedId)
  db.collection('logs').insertOne({
    rfid: scannedId,
    date: new Date(),
    energyConsumed: energyConsumed,
    roomNumber: room.roomNumber,
    status: room.status
  }, (error, result) => {
    if (error) {
      console.log(error)
    } 
  })
  response.end()
})

app.get('/getTeachers', (request, response) => {
  db.collection('teachers').find().toArray((error, documents) => {
    if (error) {
      console.log(error.message)
    } else {
      response.json(documents)
    }
  })
})

app.get('/getTeacherData', (request, response) => {
  const id = new ObjectId(request.query.id)
  db.collection('teachers').findOne({ _id: id }, (error, result) => {
    if (error) {
      console.log(error)
    } else {
      response.json(result)
    }
  })
})

app.get('/getTeacherDataByRfidCode', (request, response) => {
  const rfidCode = request.query.rfidCode
  console.log('code: ', rfidCode)
  db.collection('teachers').findOne({ rfidCode: rfidCode }, (error, result) => {
    if (error) {
      console.log(error)
    } else {
      console.log(result)
      response.json(result)
    }
  })
})

app.post('/removeTeacher', (request, response) => {
  const id = new ObjectId(request.body.id)
  console.log()
  db.collection('teachers').deleteOne({ _id: id }, (error, result) => {
    if (error) {
      console.log(error)
    } else {
      response.json(result)
    }
  })
})

app.post('/registerTeacher', (request, response) => {
  const teacher = request.body
  db.collection('teachers').insertOne({
    ...teacher
  }, (error, result) => {
    if (error) {
      console.log(error)
    } 
  })
  response.end()
})

app.get('/getRoomStatus', (request, response) => {
  db.collection('rooms').findOne({ roomNumber: '401' }, (error, result) => {
    if (error) {
      console.log(error)
    } else {
      response.json(result)
    }
  })
})

app.post('/updateRoomStatus', (request, response) => {
  const data = request.body
  db.collection('rooms').updateOne({ roomNumber: data.roomNumber }, { 
    $set: { status: data.status } 
  }, (error, result) => {
    if (error) {
      console.log(error)
    } 
  })
  response.end()
})

app.post('/updateTeacherData', (request, response) => {
  const data = request.body
  const id = data._id
  delete data._id
  db.collection('teachers').updateOne({ _id : new ObjectId(id) }, { 
    $set: { ...data } 
  }, (error, result) => {
    if (error) {
      console.log(error)
    } 
  })
  response.end()
})

app.get('/getLogs', (request, response) => {
  db.collection('logs').find().toArray(async (error, documents) => {
    if (error) {
      console.log(error.message)
    } else {
      response.json(documents)
    }
  })
})

app.post('/removeUserLogs', (request, response) => {
  const rfidCode = request.body.rfidCode
  db.collection('logs').deleteMany({ rfid: rfidCode }, (error, result) => {
    if (error) {
      console.log(error.message)
    } else {
      response.json(result)
    }
  })
})

app.listen(port, () => {
  console.log(`App has started on port: ${port}`)
})

import ipc from 'node-ipc'
import { fork } from 'child_process'
import path from 'path'

const scanner = path.resolve('./scanner.js')
fork(scanner)
let scannedId = ''
let energyConsumed = 0

ipc.config.id = 'server'
ipc.config.retry = 1500
ipc.config.silent = true
ipc.serve(() => {
  ipc.server.on('scannedid', (data) => {
    scannedId = data
    console.log('data', data)
    setTimeout(() => {
      scannedId = ''
    }, 200)
  })
  ipc.server.on('energy', (data) => {
    energyConsumed = data
    console.log('energy', data)
    setTimeout(() => {
      energyConsumed = ''
    }, 1000)
  })
})
ipc.server.start()
